## Api para la gestion de datos de Prueba Backend Youse

## Entorno local
Ejecutar Bundle y configurar base de datos, ejecutar migracion y semillas

```sh
$ bundle install
$ rake db:migrate
$ rake db:seed


```

Controladores y modelos configurados sobre namespaces API::V1::

Autenticado mediante devise_token_auth


Desarrollado sobre Cloud9 IDE

1. Crear cuenta en Cloud9.io
2. Solicitar al propietario acceso a la app https://youse-backend-yasuarez.c9users.io/ 
3. Prender servicio postgresql $sudo service postgresql start
4. Disfruta programando :)

Visit http://docs.c9.io for support, or to learn more about using Cloud9 IDE. 
To watch some training videos, visit http://www.youtube.com/user/c9ide