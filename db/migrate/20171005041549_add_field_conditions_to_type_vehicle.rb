class AddFieldConditionsToTypeVehicle < ActiveRecord::Migration
  def change
    add_column :type_vehicles, :conditions, :text
  end
end
