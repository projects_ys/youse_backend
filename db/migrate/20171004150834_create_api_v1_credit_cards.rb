class CreateApiV1CreditCards < ActiveRecord::Migration
  def change
    create_table :credit_cards do |t|
      t.integer :number
      t.string :name
      t.date :date_expiration
      t.integer :securitycode

      t.timestamps null: false
    end
  end
end
