class CreateApiV1Owners < ActiveRecord::Migration
  def change
    create_table :owners do |t|
      t.string :type_document
      t.string :document_number
      t.string :string
      t.string :names
      t.string :string
      t.string :lastnames
      t.string :string
      t.string :email
      t.string :string
      t.string :mobilphone
      t.string :string

      t.timestamps null: false
    end
  end
end
