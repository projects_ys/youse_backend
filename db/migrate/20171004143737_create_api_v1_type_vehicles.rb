class CreateApiV1TypeVehicles < ActiveRecord::Migration
  def change
    create_table :type_vehicles do |t|
      t.integer :code_type
      t.string :classvehicle
      t.string :subtype
      t.integer :age_lower
      t.integer :age_upper
      t.float :commercial_rate
      t.integer :value_cousing
      t.integer :contribution_fosyga
      t.integer :subtotal_cousing_fosyga
      t.integer :runt_rate
      t.integer :total_pay

      t.timestamps null: false
    end
  end
end
