class CreateApiV1Vehicles < ActiveRecord::Migration
  def change
    create_table :vehicles do |t|
      t.string :plate
      t.string :classvehicle
      t.integer :code
      t.integer :years
      t.integer :passengers
      t.integer :cylinder
      t.integer :tons
      t.references :owner, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
