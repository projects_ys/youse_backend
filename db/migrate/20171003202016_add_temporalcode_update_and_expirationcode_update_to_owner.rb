class AddTemporalcodeUpdateAndExpirationcodeUpdateToOwner < ActiveRecord::Migration
  def change
    add_column :owners, :temporalcode_update, :integer
    add_column :owners, :expirationcode_update, :timestamp
  end
end
