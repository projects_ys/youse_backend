class CreateApiV1Insures < ActiveRecord::Migration
  def change
    create_table :insures do |t|
      t.datetime :start_date
      t.datetime :end_time
      t.references :vehicle, index: true, foreign_key: true
      t.references :purchase, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
