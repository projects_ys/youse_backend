class ChangeColumnNameVehicles < ActiveRecord::Migration
  def change
    rename_column :vehicles, :years, :model
    remove_column :vehicles, :code
  end
end
