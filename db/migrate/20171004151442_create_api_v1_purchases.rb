class CreateApiV1Purchases < ActiveRecord::Migration
  def change
    create_table :purchases do |t|
      t.integer :number_dues
      t.references :credit_card, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
