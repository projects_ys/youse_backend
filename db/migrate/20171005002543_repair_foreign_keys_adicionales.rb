class RepairForeignKeysAdicionales < ActiveRecord::Migration
  def change
    #type_vehicle -> vehicles
    remove_foreign_key :vehicles,:type_vehicle
    add_foreign_key :vehicles, :type_vehicles, {on_update: :cascade,on_delete: :cascade}
    
    #credit_card -> purchases
    remove_foreign_key :purchases,:credit_card
    add_foreign_key :purchases, :credit_cards, {on_update: :cascade,on_delete: :cascade}
    
    #credit_card -> purchases
    remove_foreign_key :purchases,:credit_card
    add_foreign_key :purchases, :credit_cards, {on_update: :cascade,on_delete: :cascade}
    
    #vehicle -> insures
    remove_foreign_key :insures,:vehicle
    add_foreign_key :insures, :vehicles, {on_update: :cascade,on_delete: :cascade}
    
    #vehicle -> insures
    remove_foreign_key :insures,:purchase
    add_foreign_key :insures, :purchases, {on_update: :cascade,on_delete: :cascade}
    
    #owners -> user
    remove_foreign_key :owners, :user
    add_foreign_key :owners, :users, {on_update: :cascade,on_delete: :cascade}
    
    #owners -> user
    remove_foreign_key :owners, :user
    add_foreign_key :owners, :users, {on_update: :cascade,on_delete: :cascade}
    
    #owners -> user
    remove_foreign_key :credit_cards, :user
    add_foreign_key :credit_cards, :users, {on_update: :cascade,on_delete: :cascade}
    
  end
end
