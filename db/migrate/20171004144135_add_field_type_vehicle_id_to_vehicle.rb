class AddFieldTypeVehicleIdToVehicle < ActiveRecord::Migration
  def change
    add_reference :vehicles, :type_vehicle, index: true, foreign_key: true
  end
end
