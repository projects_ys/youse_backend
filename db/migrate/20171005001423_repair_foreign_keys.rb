class RepairForeignKeys < ActiveRecord::Migration
  def change
    remove_foreign_key :vehicles,:owner
    add_foreign_key :vehicles, :owners, {on_update: :cascade,on_delete: :cascade}
  end
end
