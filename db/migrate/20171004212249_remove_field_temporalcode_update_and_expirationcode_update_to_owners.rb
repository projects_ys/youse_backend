class RemoveFieldTemporalcodeUpdateAndExpirationcodeUpdateToOwners < ActiveRecord::Migration
  def change
    remove_column :owners, :expirationcode_update, :datetime
    remove_column :owners, :temporalcode_update, :integer
  end
end
