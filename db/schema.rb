# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171005134140) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "credit_cards", force: :cascade do |t|
    t.integer  "number"
    t.string   "name"
    t.date     "date_expiration"
    t.integer  "securitycode"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "user_id"
  end

  add_index "credit_cards", ["user_id"], name: "index_credit_cards_on_user_id", using: :btree

  create_table "insures", force: :cascade do |t|
    t.datetime "start_date"
    t.datetime "end_time"
    t.integer  "vehicle_id"
    t.integer  "purchase_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "insures", ["purchase_id"], name: "index_insures_on_purchase_id", using: :btree
  add_index "insures", ["vehicle_id"], name: "index_insures_on_vehicle_id", using: :btree

  create_table "owners", force: :cascade do |t|
    t.string   "type_document"
    t.string   "document_number"
    t.string   "names"
    t.string   "lastnames"
    t.string   "email"
    t.string   "mobilphone"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "user_id"
  end

  add_index "owners", ["user_id"], name: "index_owners_on_user_id", using: :btree

  create_table "purchases", force: :cascade do |t|
    t.integer  "number_dues"
    t.integer  "credit_card_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "purchases", ["credit_card_id"], name: "index_purchases_on_credit_card_id", using: :btree

  create_table "type_vehicles", force: :cascade do |t|
    t.integer  "code_type"
    t.string   "classvehicle"
    t.string   "subtype"
    t.integer  "age_lower"
    t.integer  "age_upper"
    t.float    "commercial_rate"
    t.integer  "value_cousing"
    t.integer  "contribution_fosyga"
    t.integer  "subtotal_cousing_fosyga"
    t.integer  "runt_rate"
    t.integer  "total_pay"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.text     "conditions"
  end

  create_table "users", force: :cascade do |t|
    t.string   "provider",               default: "email", null: false
    t.string   "uid",                    default: "",      null: false
    t.string   "encrypted_password",     default: "",      null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,       null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.string   "name"
    t.string   "nickname"
    t.string   "image"
    t.string   "email"
    t.json     "tokens"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["uid", "provider"], name: "index_users_on_uid_and_provider", unique: true, using: :btree

  create_table "vehicles", force: :cascade do |t|
    t.string   "plate"
    t.string   "classvehicle"
    t.integer  "model"
    t.integer  "passengers"
    t.integer  "cylinder"
    t.integer  "tons"
    t.integer  "owner_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "type_vehicle_id"
  end

  add_index "vehicles", ["owner_id"], name: "index_vehicles_on_owner_id", using: :btree
  add_index "vehicles", ["type_vehicle_id"], name: "index_vehicles_on_type_vehicle_id", using: :btree

  add_foreign_key "credit_cards", "users", on_update: :cascade, on_delete: :cascade
  add_foreign_key "insures", "purchases", on_update: :cascade, on_delete: :cascade
  add_foreign_key "insures", "vehicles", on_update: :cascade, on_delete: :cascade
  add_foreign_key "owners", "users", on_update: :cascade, on_delete: :cascade
  add_foreign_key "purchases", "credit_cards", on_update: :cascade, on_delete: :cascade
  add_foreign_key "vehicles", "owners", on_update: :cascade, on_delete: :cascade
  add_foreign_key "vehicles", "type_vehicles", on_update: :cascade, on_delete: :cascade
end
