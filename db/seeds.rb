# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
types = Api::V1::TypeVehicle.all
if types.empty?
    types_soat = [
        {
            code_type: 110,
            classvehicle: 'Motos',
            subtype: 'Menos de 100 c.c.',
            age_lower: nil,
            age_upper: nil,
            commercial_rate: 8.26,
            value_cousing: 203100,
            contribution_fosyga: 101550,
            subtotal_cousing_fosyga: 304650,
            runt_rate: 1610,
            total_pay: 306260,
            conditions: [{"cylinder":{"lower":0,"upper":100}}]
        },
        {
            code_type: 120,
            classvehicle: 'Motos',
            subtype: 'De 100 a 200 c.c.',
            age_lower: nil,
            age_upper: nil,
            commercial_rate: 11.09,
            value_cousing: 272700,
            contribution_fosyga: 136350,
            subtotal_cousing_fosyga: 409050,
            runt_rate: 1610,
            total_pay: 410660,
            conditions: [{"cylinder":{"lower":100,"upper":200}}]
        },
        {
            code_type: 130,
            classvehicle: 'Motos',
            subtype: 'Más de 200 c.c. ',
            age_lower: nil,
            age_upper: nil,
            commercial_rate: 12.51,
            value_cousing: 307600,
            contribution_fosyga: 153800,
            subtotal_cousing_fosyga: 461400,
            runt_rate: 1610,
            total_pay: 463010,
            conditions: [{"cylinder":{"lower":200}}]
        },
    ]
    Api::V1::TypeVehicle.create(types_soat)
end