require 'rails_helper'

RSpec.describe Api::V1::TypesvehiclesController, type: :controller do

  describe "GET #distinct" do
    it "returns http success" do
      get :distinct
      expect(response).to have_http_status(:success)
    end
  end

end
