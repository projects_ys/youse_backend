require 'rails_helper'

RSpec.describe Api::V1::TypeVehicle, type: :model do
    it "should have many teams" do
        t = Api::V1::TypeVehicle.reflect_on_association(:vehicles)
        expect(t.macro).to eq(:has_many)
    end
end
