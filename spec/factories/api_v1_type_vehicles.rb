FactoryGirl.define do
  factory :api_v1_type_vehicle, class: 'Api::V1::TypeVehicle' do
    code_type 1
    classvehicle "MyString"
    subtype "MyString"
    age_lower 1
    age_upper 1
    commercial_rate 1.5
    value_cousing 1
    contribution_fosyga 1
    subtotal_cousing_fosyga 1
    runt_rate 1
    total_pay 1
  end
end
