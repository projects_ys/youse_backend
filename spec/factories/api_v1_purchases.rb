FactoryGirl.define do
  factory :api_v1_purchase, class: 'Api::V1::Purchase' do
    number_dues 1
    credit_card nil
  end
end
