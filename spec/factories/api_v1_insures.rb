FactoryGirl.define do
  factory :api_v1_insure, class: 'Api::V1::Insure' do
    start_date "2017-10-04 15:18:03"
    end_time "2017-10-04 15:18:03"
    vehicle nil
    purchase nil
  end
end
