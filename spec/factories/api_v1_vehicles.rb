FactoryGirl.define do
  factory :api_v1_vehicle, class: 'Api::V1::Vehicle' do
    plate "MyString"
    classvehicle "MyString"
    code 1
    years 1
    passengers 1
    cylinder 1
    tons 1
    owner nil
  end
end
