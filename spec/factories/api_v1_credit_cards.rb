FactoryGirl.define do
  factory :api_v1_credit_card, class: 'Api::V1::CreditCard' do
    number 1
    name "MyString"
    date_expiration "2017-10-04"
    securitycode 1
  end
end
