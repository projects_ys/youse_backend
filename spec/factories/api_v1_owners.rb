FactoryGirl.define do
  factory :api_v1_owner, class: 'Api::V1::Owner' do
    type_document "MyString"
    document_number "MyString"
    string "MyString"
    names "MyString"
    string "MyString"
    lastnames "MyString"
    string "MyString"
    email "MyString"
    string "MyString"
    mobilphone "MyString"
    string "MyString"
  end
end
