require 'rails_helper'

RSpec.describe "api/v1s/edit", type: :view do
  before(:each) do
    @api_v1 = assign(:api_v1, Api::V1::Owner.create!(
      :type_document => "MyString",
      :document_number => "MyString",
      :string => "MyString",
      :names => "MyString",
      :string => "MyString",
      :lastnames => "MyString",
      :string => "MyString",
      :email => "MyString",
      :string => "MyString",
      :mobilphone => "MyString",
      :string => "MyString"
    ))
  end

  it "renders the edit api_v1 form" do
    render

    assert_select "form[action=?][method=?]", api_v1_path(@api_v1), "post" do

      assert_select "input#api_v1_type_document[name=?]", "api_v1[type_document]"

      assert_select "input#api_v1_document_number[name=?]", "api_v1[document_number]"

      assert_select "input#api_v1_string[name=?]", "api_v1[string]"

      assert_select "input#api_v1_names[name=?]", "api_v1[names]"

      assert_select "input#api_v1_string[name=?]", "api_v1[string]"

      assert_select "input#api_v1_lastnames[name=?]", "api_v1[lastnames]"

      assert_select "input#api_v1_string[name=?]", "api_v1[string]"

      assert_select "input#api_v1_email[name=?]", "api_v1[email]"

      assert_select "input#api_v1_string[name=?]", "api_v1[string]"

      assert_select "input#api_v1_mobilphone[name=?]", "api_v1[mobilphone]"

      assert_select "input#api_v1_string[name=?]", "api_v1[string]"
    end
  end
end
