require 'rails_helper'

RSpec.describe "api/v1s/show", type: :view do
  before(:each) do
    @api_v1 = assign(:api_v1, Api::V1::Owner.create!(
      :type_document => "Type Document",
      :document_number => "Document Number",
      :string => "String",
      :names => "Names",
      :string => "String",
      :lastnames => "Lastnames",
      :string => "String",
      :email => "Email",
      :string => "String",
      :mobilphone => "Mobilphone",
      :string => "String"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Type Document/)
    expect(rendered).to match(/Document Number/)
    expect(rendered).to match(/String/)
    expect(rendered).to match(/Names/)
    expect(rendered).to match(/String/)
    expect(rendered).to match(/Lastnames/)
    expect(rendered).to match(/String/)
    expect(rendered).to match(/Email/)
    expect(rendered).to match(/String/)
    expect(rendered).to match(/Mobilphone/)
    expect(rendered).to match(/String/)
  end
end
