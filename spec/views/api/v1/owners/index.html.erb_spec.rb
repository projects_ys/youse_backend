require 'rails_helper'

RSpec.describe "api/v1s/index", type: :view do
  before(:each) do
    assign(:api_v1_owners, [
      Api::V1::Owner.create!(
        :type_document => "Type Document",
        :document_number => "Document Number",
        :string => "String",
        :names => "Names",
        :string => "String",
        :lastnames => "Lastnames",
        :string => "String",
        :email => "Email",
        :string => "String",
        :mobilphone => "Mobilphone",
        :string => "String"
      ),
      Api::V1::Owner.create!(
        :type_document => "Type Document",
        :document_number => "Document Number",
        :string => "String",
        :names => "Names",
        :string => "String",
        :lastnames => "Lastnames",
        :string => "String",
        :email => "Email",
        :string => "String",
        :mobilphone => "Mobilphone",
        :string => "String"
      )
    ])
  end

  it "renders a list of api/v1s" do
    render
    assert_select "tr>td", :text => "Type Document".to_s, :count => 2
    assert_select "tr>td", :text => "Document Number".to_s, :count => 2
    assert_select "tr>td", :text => "String".to_s, :count => 2
    assert_select "tr>td", :text => "Names".to_s, :count => 2
    assert_select "tr>td", :text => "String".to_s, :count => 2
    assert_select "tr>td", :text => "Lastnames".to_s, :count => 2
    assert_select "tr>td", :text => "String".to_s, :count => 2
    assert_select "tr>td", :text => "Email".to_s, :count => 2
    assert_select "tr>td", :text => "String".to_s, :count => 2
    assert_select "tr>td", :text => "Mobilphone".to_s, :count => 2
    assert_select "tr>td", :text => "String".to_s, :count => 2
  end
end
