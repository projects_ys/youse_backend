Rails.application.routes.draw do

  mount_devise_token_auth_for 'User', at: 'auth'
  namespace :api do
    namespace :v1 do
      resources :owners, only: [:index,:show, :create, :update,:destroy], defaults: { format: 'json' }
      resources :vehicles, only: [:index,:show, :create, :update,:destroy], defaults: { format: 'json' } do
        get 'with_soat'
      end
      get 'typesvehicles/distinct', defaults: { format: 'json' }
    end
  end

end
