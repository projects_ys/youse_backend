class Api::V1::OwnersController < ApplicationController
  before_action :set_owner, only: [:show, :update,:destroy]
  before_filter :authenticate_user!
  
  def index
    @owners = Api::V1::Owner.where(user_id: current_user.id).as_json
    @number_status = 200
    render json: @owners,status: @number_status
  end
  
  def show
    render json: @owner
  end

  def create
    @owner = Api::V1::Owner.new(owner_params)
    @owner.user_id = current_user.id
    if @owner.save
      render json: { status: :created, entity: @owner }
    else
      render json: { errors: @owner.errors, status: :unprocessable_entity }
    end
  end

  def update
    if @owner.update(owner_params)
       render json: { status: :ok, entity: @owner }
    else
      render json: { errors: @owner.errors, status: :unprocessable_entity }
    end
  end
  
  def destroy
    if @owner.delete
      render json: {status: :ok}
    else
      render json: {errors: @owner.errors, status: :unprocessable_entity }
    end
  rescue => e
    render json: {exception: e.message}
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_owner
      @owner = Api::V1::Owner.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def owner_params
      params.require(:owner).permit(:type_document, :document_number, :string, :names, :string, :lastnames, :string, :email, :string, :mobilphone, :string)
    end
end
