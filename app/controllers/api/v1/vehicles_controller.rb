class Api::V1::VehiclesController < ApplicationController
  before_action :set_vehicle, only: [:show, :update,:destroy]
  before_filter :authenticate_user!
  
  def index
    owners = Api::V1::Owner.where(user_id: current_user.id)
    vehicles = Api::V1::Vehicle.where(owner_id: owners).as_json(include_info_owner)
    number_status = 200
    render json: vehicles,status: number_status
  end

  def show
    render json: @vehicle
  end
  
  def with_soat
    @vehicle = Api::V1::Vehicle.where('lower(plate) = ?', params[:vehicle_id].downcase).first
    render json: @vehicle.as_json(include:[:insures])
  end

  def create
    @vehicle = Api::V1::Vehicle.new(vehicle_params)
    if @vehicle.save
      @vehicle.detect_type
      render json: { status: :created, entity: @vehicle }
    else
      render json: { errors: @vehicle.errors, status: :unprocessable_entity }
    end
  end

  def update
    if @vehicle.update(vehicle_params)
      @vehicle.detect_type
      render json: { status: :ok, entity: @vehicle }
    else
      render json: { errors: @vehicle.errors, status: :unprocessable_entity }
    end
  end
  
  def destroy
    if @vehicle.delete
      render json: {status: :ok}
    else
      render json: {errors: @vehicle.errors, status: :unprocessable_entity }
    end
  rescue => e
    render json: {exception: e.message}
  end
  
  private 
  def set_vehicle
     @vehicle = Api::V1::Vehicle.find(params[:id])
  end
  def vehicle_params
    params.require(:vehicle).permit(:plate,:classvehicle,:model,:passengers,:cylinder,:tons,:owner_id)
  end
  def include_info_owner
    {only:[:id,:plate,:classvehicle,:model, :passengers, :cylinder, :tons],include:[:type_vehicle]}
  end
end
