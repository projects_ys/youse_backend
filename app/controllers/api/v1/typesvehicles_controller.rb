class Api::V1::TypesvehiclesController < ApplicationController
  def distinct
    types_vehicles = Api::V1::TypeVehicle.select("classvehicle").group("classvehicle")
    render json: types_vehicles
  end
end
