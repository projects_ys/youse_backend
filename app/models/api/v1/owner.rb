class Api::V1::Owner < ActiveRecord::Base
    self.table_name = "owners"
    has_many :vehicles
    belongs_to :owner
end
