class Api::V1::Vehicle < ActiveRecord::Base
  self.table_name = "vehicles"
  has_many :insures
  belongs_to :owner
  belongs_to :type_vehicle
  def detect_type
    resultstype = Api::V1::TypeVehicle.where(classvehicle: self.classvehicle)
    resultstype.each_with_index do |value,index|
      completed_to_condition = value.conditions.length
      value.conditions.each do |condition|
        @cumple = 0
        if condition[:cylinder].present?
          cylinderApproval = false
          unless condition[:cylinder][:lower].present?
            condition[:cylinder][:lower] = 0
          end
          unless condition[:cylinder][:upper].present?
            condition[:cylinder][:upper] = 5000000
          end
          if self.cylinder >= condition[:cylinder][:lower] && self.cylinder <= condition[:cylinder][:upper]
            @cumple = @cumple + 1
            break
          end
        end
      end
      if @cumple == completed_to_condition
        self.type_vehicle_id = value.id
        self.save
        break
      end
    end
  end
end
