class Api::V1::Insure < ActiveRecord::Base
  self.table_name = 'insures'
  belongs_to :vehicle
  belongs_to :purchase
end
