class Api::V1::CreditCard < ActiveRecord::Base
    self.table_name = 'credit_cards'
    has_many :purchases
    belongs_to :user
end
