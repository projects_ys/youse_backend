class Api::V1::Purchase < ActiveRecord::Base
    self.table_name = 'purchases'
    has_many :insures
    belongs_to :credit_card
end
