class Api::V1::TypeVehicle < ActiveRecord::Base
    self.table_name = 'type_vehicles'
    serialize :conditions
    has_many :vehicles
end
